﻿// See https://aka.ms/new-console-template for more information


using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization.Conventions;

using Restaurants;
using MongoDB.Bson;

internal class Program
{

    private const string connectionString = "mongodb+srv://spravce:JYHoYzXaRFMvZah5@learn-mongodb-cluster.lkbhbsw.mongodb.net/?retryWrites=true&w=majority";

    //private static IMongoCollection<Restaurant>? restaurantCollection;    // do i need that?
    private static void Main(string[] args)
    {
                
        if(string.IsNullOrEmpty(connectionString))
        {
            
            Console.WriteLine("You must set your 'MONGODB_URI' environment variable. To learn how to set it, see https://www.mongodb.com/docs/drivers/csharp/current/quick-start/#set-your-connection-string");
            Environment.Exit(0);

        }

        ConventionPack camelCaseConvention = new ConventionPack { new CamelCaseElementNameConvention() };

        ConventionRegistry.Register("camelCase", camelCaseConvention, type => true);

        var client = new MongoClient(connectionString);

        string database = "sample_restaurants";
        string table = "restaurants";

        var restaurantCollection = client.GetDatabase(database).GetCollection<Restaurant>(table);

// Builders
        //var filter = Builders<Restaurant>.Filter.Eq(restaurant => restaurant.Name, "Bagels N Buns");

        FilterDefinition<Restaurant> filter = 
            Builders<Restaurant>.Filter.Eq(restarant => restarant.Name, "Bagels N Buns");

        var restaurant = restaurantCollection.Find(filter).FirstOrDefault();
        
        //Console.WriteLine(restaurant);
        Console.WriteLine(restaurant.Name);

        //Pizza to list
        filter = Builders<Restaurant>.Filter
            .Eq(restaurant => restaurant.Cuisine, "Pizza");

        var restaurants = restaurantCollection.Find(filter).ToList();     //sync

        Console.WriteLine($"Number of restauants burning pizza found { restaurants.Count() }"); 

        if (restaurants.Count() > 0)
        {

            //restaurants.ForEach(restaurant => Console.WriteLine(restaurant.ToBsonDocument()));

            restaurants.ForEach(restaurant => Console.WriteLine($"Restaurant name (Lambda) { restaurant.ToBsonDocument() }" ));
        
        }

        if (restaurants.Count() > 0)
        {
            
            foreach (var r in restaurants)
            {
                
                //Console.WriteLine($"Restaurant name (ForEach) { restaurantName }");
                //Console.WriteLine($"Restaurant name (ForEach) { r.Name }");
                Console.WriteLine($"Restaurant name (ForEach) { r.ToBsonDocument() }");
                
            }

        }
        
// LINQ        
        var queryableCollection = restaurantCollection.AsQueryable();

        var query = queryableCollection.AsQueryable()
            .Where(restaurant => restaurant.Name == "Bagels N Buns").FirstOrDefault();

        Console.WriteLine(query.ToBsonDocument());
        
    }

}
