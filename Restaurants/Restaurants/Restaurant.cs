﻿namespace Restaurants;

using MongoDB.Bson;                                 // needed for ObjectId
using MongoDB.Bson.Serialization.Attributes;
//using MongoDB.Bson.Serialization.Conventions;

class Restaurant
{
    public ObjectId Id { get; set; }

    public string? Name { get; set; }

    [BsonElement("restaurant_id")]
    public string? RestaurantId { get; set; }

    public string? Cuisine { get; set; }

    public Address? Address { get; set; }

    public string? Borough { get; set; }

    public List<GradeEntry>? Grades { get; set; }
}